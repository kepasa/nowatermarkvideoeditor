This is simple video editor for Android.
It enables user to cut and concatenate videos, view edited sequence in real time and store rendered sequence on internal memory.
Videos are processed using open source FFMpeg.

