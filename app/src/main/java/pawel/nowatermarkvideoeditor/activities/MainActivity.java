package pawel.nowatermarkvideoeditor.activities;


import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.util.Pair;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;
import android.widget.VideoView;

import com.woxthebox.draglistview.DragListView;

import org.florescu.android.rangeseekbar.RangeSeekBar;

import java.util.List;

import pawel.nowatermarkvideoeditor.videolist.MySwipeRefreshLayout;
import pawel.nowatermarkvideoeditor.R;
import pawel.nowatermarkvideoeditor.videolist.VideoList;
import pawel.nowatermarkvideoeditor.videoprocessor.VideoProcessor;

public class MainActivity extends AppCompatActivity {

    private static String TAG = "NVE_MainActivity";
    private static final int MY_PERMISSION_REQUEST_WRITE_EXTERNAL_STORAGE =  150;

    /// video processor
    private VideoProcessor mVideoProcessor;


    /// video list
    private VideoList mVideoList;

    private SwipeRefreshLayout mSwipeLayout;
    private ConstraintLayout mConstraintLayout;
    private RangeSeekBar<Long> mRangeSeekBar;
    private Button mButton;
    private VideoView mVideoView;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);



        mVideoView = (VideoView) findViewById(R.id.videoView);
        mVideoProcessor = new VideoProcessor(this, mVideoView);
        mVideoList = new VideoList(this, (DragListView)findViewById(R.id.drag_list_view), (MySwipeRefreshLayout) findViewById(R.id.swipe_refresh_layout));
        mSwipeLayout = (SwipeRefreshLayout) findViewById(R.id.swipe_refresh_layout);
        mConstraintLayout = (ConstraintLayout) findViewById(R.id.constraintLayout);
        mButton = (Button) findViewById(R.id.button);
        mRangeSeekBar = (RangeSeekBar<Long>) findViewById(R.id.range_seek_bar);

        requestPermissions();
        setupLayouts();
    }



    @Override
    public boolean onCreateOptionsMenu(Menu menu){
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.menu_bar, menu);
        return true;
    }

    public void setupLayouts(){
        mSwipeLayout.setVisibility(View.VISIBLE);
        mConstraintLayout.setVisibility(View.GONE);
    }


    private void createOpenFileIntent(){
        Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Video.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(intent, 10);
    }

    public void stopPlayback(){
        mVideoProcessor.stopPlayback();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data){
        if(resultCode == RESULT_OK && requestCode == 10) {
            Uri uriMovie = data.getData();
            Long id = mVideoList.addElement();
            mVideoProcessor.addVideo(id, uriMovie);
           // mVideoProcessor.play(this, uriMovie);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item){
        switch(item.getItemId()) {
            case R.id.action_render:
                Toast.makeText(this, "Render selected", Toast.LENGTH_SHORT).show();
                mVideoProcessor.initiazeRender(mVideoList.getItems());
                break;
            case R.id.action_open:
                Toast.makeText(this, "Open selected", Toast.LENGTH_SHORT).show();
                createOpenFileIntent();
                break;
            case R.id.action_play:
                Toast.makeText(this, "Play selected", Toast.LENGTH_SHORT).show();
                playVideos();
                break;
            default:
                break;
        }
        return true;
    }

    private void playVideos(){
        List videosId = mVideoList.getItems();
        mVideoProcessor.initiazePlay(videosId);
        mVideoProcessor.play();
    }

    public void requestPermissions(){
        // Here, thisActivity is the current activity
        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.WRITE_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {

            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE)) {

                // Show an explanation to the user *asynchronously* -- don't block
                // this thread waiting for the user's response! After the user
                // sees the explanation, try again to request the permission.

            } else {

                // No explanation needed, we can request the permission.

                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                        MY_PERMISSION_REQUEST_WRITE_EXTERNAL_STORAGE);

                // MY_PERMISSIONS_REQUEST_READ_CONTACTS is an
                // app-defined int constant. The callback method gets the
                // result of the request.
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSION_REQUEST_WRITE_EXTERNAL_STORAGE: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.

                } else {

                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                }
                return;
            }

            // other 'case' lines to check for other
            // permissions this app might request
        }
    }

    public void handleItemClicked(long pos){
        mSwipeLayout.setVisibility(View.GONE);
        mConstraintLayout.setVisibility(View.VISIBLE);
        Pair<Long, String> vId = mVideoList.getItemAtPosition(pos);
        Log.d(TAG, "got video id: " + vId.first);

        final VideoProcessor.VideoHolder video =  mVideoProcessor.getVideoAtId(vId.first);

        Log.d(TAG, "processor returned vId: " + video.getId() + " duration " + video.getDuration() + " start: " + video.getStart() + " finish: " + video.getEnd());

        mRangeSeekBar.resetSelectedValues();
        mRangeSeekBar.setRangeValues((long) 0, video.getDuration());
        mRangeSeekBar.setSelectedMinValue(video.getStart());
        mRangeSeekBar.setSelectedMaxValue(video.getEnd());
        mButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setupLayouts();
                //todo check difference
                long min = mRangeSeekBar.getSelectedMinValue();
                long max  =  mRangeSeekBar.getSelectedMaxValue();
                Log.d(TAG, "commiting changes: " + min + " " + max);
                video.updateRange(min, max);
            }
        });
    }

    public void removeUri(long pos){
        mVideoProcessor.removeUri(pos);
    }
}
