package pawel.nowatermarkvideoeditor.videolist;
import android.animation.ObjectAnimator;
import android.content.Context;
import android.support.v7.widget.CardView;
import android.view.View;
import android.view.animation.DecelerateInterpolator;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.woxthebox.draglistview.DragItem;

import pawel.nowatermarkvideoeditor.R;

class MyDragItem extends DragItem {

    MyDragItem(Context context, int layoutId) {
        super(context, layoutId);
    }

    @Override
    public void onBindDragView(View clickedView, View dragView) {
        CharSequence text = ((TextView) clickedView.findViewById(R.id.text)).getText();
        ((TextView) dragView.findViewById(R.id.text)).setText(text);
        dragView.findViewById(R.id.item_layout).setBackgroundColor(dragView.getResources().getColor(R.color.list_item_background));
    }


}