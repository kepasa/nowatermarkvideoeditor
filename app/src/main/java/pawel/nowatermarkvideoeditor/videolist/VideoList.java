package pawel.nowatermarkvideoeditor.videolist;

import android.support.v4.content.ContextCompat;
import android.support.v4.util.Pair;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;

import com.woxthebox.draglistview.DragListView;
import com.woxthebox.draglistview.swipe.ListSwipeHelper;
import com.woxthebox.draglistview.swipe.ListSwipeItem;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

import pawel.nowatermarkvideoeditor.R;
import pawel.nowatermarkvideoeditor.activities.MainActivity;

/**
 * Created by pawel on 24.06.17.
 */

public class VideoList {
    private static String TAG = "NWVE_LIST";
    private MainActivity mainActivity;
    private ArrayList<Pair<Long, String>> mItemArray;
    private DragListView mDragListView;
    private ListSwipeHelper mSwipeHelper;
    private MySwipeRefreshLayout mRefreshLayout;
    private long mMovieCounter = 0;
    private ItemAdapter listAdapter;

    public VideoList(MainActivity mainActivity, DragListView dragListView, MySwipeRefreshLayout mySwipeRefreshLayout){
        this.mainActivity = mainActivity;
        this.mDragListView = dragListView;
        this.mRefreshLayout = mySwipeRefreshLayout;
        configureList();
    }

    public long addElement(){
        ++mMovieCounter;
        listAdapter.addItem(listAdapter.getItemCount(), new Pair<>((long) mMovieCounter, "Movie " + mMovieCounter));
        return mMovieCounter;
    }


    void configureList(){
        mDragListView.getRecyclerView().setVerticalScrollBarEnabled(true);
        mDragListView.setDragListListener(new DragListView.DragListListenerAdapter() {
            //// TODO: 28.06.2017 stop playback
            @Override
            public void onItemDragStarted(int position) {
                mainActivity.stopPlayback();
                mRefreshLayout.setEnabled(false);
                //Toast.makeText(mDragListView.getContext(), "Start - position: " + position, Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onItemDragEnded(int fromPosition, int toPosition) {
                mRefreshLayout.setEnabled(true);
                if (fromPosition != toPosition) {
                    //Toast.makeText(mDragListView.getContext(), "End - position: " + toPosition, Toast.LENGTH_SHORT).show();
                }
            }
        });

        mItemArray = new ArrayList<>();

        mRefreshLayout.setScrollingView(mDragListView.getRecyclerView());
        mRefreshLayout.setColorSchemeColors(ContextCompat.getColor(mainActivity, R.color.app_color));

        // Pretty disgusting but has to be done in order to disable annoying refresh spinner when swiping down
        try{
            Field mRefreshing = SwipeRefreshLayout.class.getDeclaredField("mRefreshing");
            mRefreshing.setAccessible(true);
            mRefreshing.set(mRefreshLayout, Boolean.TRUE);
        }catch (NoSuchFieldException | IllegalAccessException ex){
            Log.e(TAG, "Error while trying to cancel refresh" + ex);
        }

        mDragListView.setSwipeListener(new ListSwipeHelper.OnSwipeListenerAdapter() {
            @Override
            public void onItemSwipeStarted(ListSwipeItem item) {
                mRefreshLayout.setEnabled(false);
            }

            @Override
            public void onItemSwipeEnded(ListSwipeItem item, ListSwipeItem.SwipeDirection swipedDirection) {
                mRefreshLayout.setEnabled(true);

                // Swipe to delete on left or right
                if (swipedDirection == ListSwipeItem.SwipeDirection.LEFT || swipedDirection == ListSwipeItem.SwipeDirection.RIGHT) {
                    Pair<Long, String> adapterItem = (Pair<Long, String>) item.getTag();
                    int pos = mDragListView.getAdapter().getPositionForItem(adapterItem);
                    mDragListView.getAdapter().removeItem(pos);
                    // remove form map uri to id
                    mainActivity.removeUri(adapterItem.first);
                }
            }
        });
        setupListRecyclerView();

    }

    public Pair<Long, String> getItemAtPosition(long pos){
        Pair<Long, String> dragItem = (Pair<Long, String>) mDragListView.getAdapter().getItemList().get(Math.toIntExact(pos));
        Log.d(TAG, "got item + " + pos + " item: " + dragItem);
        return dragItem;
    }

    public List<Pair<Long, String>> getItems(){
        List<Pair<Long, String>> itemList = mDragListView.getAdapter().getItemList();
        return itemList;
    }

    private void setupListRecyclerView() {
        mDragListView.setLayoutManager(new LinearLayoutManager(mainActivity));
        listAdapter = new ItemAdapter(mItemArray, R.layout.list_item, R.id.image, false, (MainActivity) mainActivity);
        mDragListView.setAdapter(listAdapter, true);
        mDragListView.setCanDragHorizontally(false);
        mDragListView.setCustomDragItem(new MyDragItem(mainActivity, R.layout.list_item));
    }
}
