package pawel.nowatermarkvideoeditor.videoprocessor;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.media.MediaMetadataRetriever;
import android.net.Uri;
import android.os.CountDownTimer;
import android.os.Environment;
import android.support.v4.util.Pair;
import android.util.Log;
import android.widget.MediaController;
import android.widget.Toast;
import android.widget.VideoView;

import com.github.hiteshsondhi88.libffmpeg.ExecuteBinaryResponseHandler;
import com.github.hiteshsondhi88.libffmpeg.FFmpeg;
import com.github.hiteshsondhi88.libffmpeg.LoadBinaryResponseHandler;
import com.github.hiteshsondhi88.libffmpeg.exceptions.FFmpegCommandAlreadyRunningException;
import com.github.hiteshsondhi88.libffmpeg.exceptions.FFmpegNotSupportedException;
import com.ipaulpro.afilechooser.utils.FileUtils;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import pawel.nowatermarkvideoeditor.activities.MainActivity;

/**
 * Created by pawel on 24.06.17.
 */

public class VideoProcessor {

    private static String TAG = "NWVE_PROC";
    private VideoView videoView;
    private FFmpeg ffmpeg;
    private Context mainActivity;
    private MediaController mediaController;
    private HashMap<Long, VideoHolder> mVideoMap;
    private String filePath;
    private int playedCounter = 0;
    private List<Pair<Long, String>> currentList;
    private int finishedSplits = 0;
    private List<String> sourceVideosPaths = new ArrayList<>();
    private ProgressDialog mProgressDialog;


    public VideoProcessor(Context mainActivity, VideoView videoView){
        this.mainActivity = mainActivity;
        this.videoView = videoView;
        mVideoMap = new HashMap<>();
        filePath =  new String();

       //mediaController = new MediaController(mainActivity);
        //mediaController.setAnchorView(videoView);
        //videoView.setMediaController(mediaController);

        loadFFMpegBinary();
    }

    public void addVideo(long id, Uri uri){
        Log.d(TAG, "uri path" + uri.getPath());
        mVideoMap.put(id, new VideoHolder(mainActivity, uri, id));
    }

    public void removeUri(long id){
        Log.d(TAG, "removed: " + mVideoMap.remove(id));
    }

    public VideoHolder getVideoAtId(long id){
        VideoHolder vh = mVideoMap.get(id);
        return vh;
    }

    private void showUnsupportedExceptionDialog(){
        Toast.makeText(mainActivity, "UNSUPPORTED EXCEPTION!", Toast.LENGTH_LONG).show();
    }
    private void loadFFMpegBinary(){
        try{
            if(ffmpeg == null) {
                Log.d(TAG, "ffmeg: null");
                ffmpeg = FFmpeg.getInstance(mainActivity);
            }
            ffmpeg.loadBinary(new LoadBinaryResponseHandler(){
                @Override
                public void onFailure(){
                    showUnsupportedExceptionDialog();
                }

                @Override
                public void onSuccess(){
                    Log.d(TAG, "ffmpeg: correct load");
                }

            });
        }catch (FFmpegNotSupportedException fme){
            showUnsupportedExceptionDialog();
        } catch (Exception e){
            Log.d(TAG, "Exception not supported: " + e);
        }
    }

    public static String[] combine(String[] arg1, String[] arg2, String[] arg3) {
        String[] result = new String[arg1.length + arg2.length + arg3.length];
        System.arraycopy(arg1, 0, result, 0, arg1.length);
        System.arraycopy(arg2, 0, result, arg1.length, arg2.length);
        System.arraycopy(arg3, 0, result, arg1.length + arg2.length, arg3.length);
        return result;
    }

    private void concatenateVideosCommand(){
        File moviesDir = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_MOVIES);
        File srcDir = new File(moviesDir, "VideoParts");
        Log.d(TAG, "PLAY VIDEO DIR: " + srcDir);
        ArrayList<File> files = new ArrayList<>();
        String path;

        /*
        for(Map.Entry<Long, VideoHolder> entry : mVideoMap.entrySet()){
            path = FileUtils.getPath(mainActivity, entry.getValue().getUri());
            Log.d(TAG, "path: " + path);
            try {
                files.add(new File(path));
            } catch (Exception e) {
                Log.e(TAG,  e.toString());
            }
        }*/
        for(String s : sourceVideosPaths){
            path = s;
            Log.d(TAG, "path: " + path);
            try {
                files.add(new File(path));
            } catch (Exception e) {
                Log.e(TAG,  e.toString());
            }
        }

        StringBuilder stringBuilder = new StringBuilder();
        StringBuilder filterComplex = new StringBuilder();
        filterComplex.append("-filter_complex,");
        for (int i = 0; i < files.size(); i++) {
            stringBuilder.append("-i" + "," + files.get(i).getAbsolutePath() + ",");
            filterComplex.append("[").append(i).append(":v").append(i).append("] [").append(i).append(":a").append(i).append("] ");

        }
        filterComplex.append("concat=n=").append(files.size()).append(":v=1:a=1 [v] [a]");

        String[] inputCommand = stringBuilder.toString().split(",");
        String[] filterCommand = filterComplex.toString().split(",");

        //TODO change prefix
        String filePrefix = "/outputVideo";
        String fileExtn = ".mp4";
        File dest =  new File(moviesDir, filePrefix + fileExtn);
        int fileNo = 0;
        while(dest.exists()) {
            fileNo++;
            dest = new File(moviesDir + filePrefix + fileNo + fileExtn);
        }

        filePath = dest.getAbsolutePath();
        Log.d(TAG, "FILE PATH: " + filePath);
        String[] destinationCommand =  {"-map", "[v]", "-map", "[a]", dest.getAbsolutePath()};
        execFFmpegBinary(combine(inputCommand, filterCommand, destinationCommand));
    }

    public static boolean deleteDir(File dir){
        if(dir.isDirectory()){
            String[] children = dir.list();
            if(children != null){
                for(int i = 0; i < children.length; ++i){
                    boolean success = deleteDir(new File(dir, children[i]));
                    if(!success)
                        return false;
                }
            }
        }
        return dir.delete();
    }


    private void splitVideoCommand(VideoHolder video){
        Log.d(TAG, "trying to cut video id: " + video.getId() + " start: " + video.getStart() +  " end " + video.getEnd());
        File moviesDir =  Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_MOVIES);
        String filePrefix = "split_video" + video.getId();
        String fileExtn = ".mp4";
        String yourRealPath = FileUtils.getPath(mainActivity, video.getUri());

        Log.d(TAG, "video real path: " + yourRealPath);

        File dir = new File(moviesDir, "VideoParts");
        /*
        if(dir.exists())
            deleteDir(dir);
        dir.mkdir();
        */
        File dest = new File(dir, filePrefix + fileExtn);
        Log.d(TAG, "dest: " + dest);
        sourceVideosPaths.add(dest.getAbsolutePath());

        String[] command = {"-ss", "" + video.getStart() / 1000, "-y", "-i", yourRealPath, "-t", "" + (video.getEnd() - video.getStart()) / 1000, "-s", "1280x720",
                "-r", "15", "-vcodec", "mpeg4", "-b:v", "2097152", "-b:a", "48000", "-ac", "2", "-ar", "22050", dest.getAbsolutePath()};

        execFFmpegBinary(command);

    }

    public void cleanWorkingDirectory(){
        File moviesDir =  Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_MOVIES);
        File dir = new File(moviesDir, "VideoParts");
        if(dir.exists())
            deleteDir(dir);
        dir.mkdir();
    }

    public void initiazePlay(List<Pair<Long, String>> list){
        cleanWorkingDirectory();
        playedCounter = 0;
        currentList = list;
        Log.d(TAG, "init play: " + list.size());
    }

    public void stopPlayback(){
        Toast.makeText(mainActivity, "Playback stopped", Toast.LENGTH_SHORT).show();
        videoView.stopPlayback();
        playedCounter = 10000000;
    }

    public void play(){
        if(playedCounter >= currentList.size()) {
            Toast.makeText(mainActivity, "Playback finished", Toast.LENGTH_SHORT).show();
            return;
        }

        Pair<Long, String> current = (Pair<Long, String>) currentList.get(playedCounter);
        VideoHolder currentVideo = getVideoAtId(current.first);
        Log.d(TAG, "playing back id: " + current.first + " start: " + currentVideo.start + " end: " + currentVideo.end);

        CountDownTimer aCounter = new CountDownTimer(currentVideo.getRealLength(), 100) {
            @Override
            public void onTick(long l) {
                //Log.d(TAG, "played back another bit, until finished: " + l);
            }

            @Override
            public void onFinish() {
                videoView.stopPlayback();
                ++playedCounter;
                play();
            }
        };
        videoView.setVideoURI(currentVideo.getUri());
        videoView.seekTo(Math.toIntExact(currentVideo.getStart()));
        videoView.start();
        aCounter.start();

    }

    public void initiazeRender(List<Pair<Long, String>> list){
        /// todo check if list has size > 0
        if(list == null || list.size() == 0){
            AlertDialog.Builder builder;
            builder = new AlertDialog.Builder(mainActivity);
            builder.setTitle("No videos loaded!").setMessage("There are no videos loaded.");
            builder.setNeutralButton("Dismiss", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    dialogInterface.cancel();
                }
            });
            AlertDialog alertDialog = builder.create();
            alertDialog.show();
            return;
        }
        cleanWorkingDirectory();
        sourceVideosPaths.clear();
        playedCounter = 10000000;
        videoView.stopPlayback();
        currentList = list;
        finishedSplits = 0;
        /// todo show progress dialog
        mProgressDialog = new ProgressDialog(mainActivity);
        mProgressDialog.setTitle("Rendering...");
        mProgressDialog.setMessage("Preparing video " + 1 + "/" + currentList.size());
        mProgressDialog.setCancelable(false);
        mProgressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
        //mProgressDialog.setIndeterminate(true);
        mProgressDialog.setMax(currentList.size() + 1);
        mProgressDialog.show();
        render(0);
    }

    public void render(final long id) {

        Runnable changeMessage = new Runnable() {
            @Override
            public void run() {
                Log.d(TAG, "updateing massage");
                mProgressDialog.setMessage("Preparing video " + (id+1) + "/" + currentList.size());
            }
        };
        ((MainActivity)mainActivity).runOnUiThread(changeMessage);

        try {
            splitVideoCommand(getVideoAtId(currentList.get((int) id).first));
            //concatenateVideosCommand();
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        } catch (SecurityException e) {
            e.printStackTrace();
        } catch (IllegalStateException e) {
            e.printStackTrace();
        }
    }


    public void execFFmpegBinary(final String[] command){
        Log.d(TAG, "trying to exec: ");

        for(String s : command){
            Log.d(TAG, "s: " + s);
        }
        try{
            ffmpeg.execute(command, new ExecuteBinaryResponseHandler(){
                @Override
                public void onFailure(String s) {
                    Log.d(TAG, "FAILED with output : " + s);
                }

                @Override
                public void onSuccess(String s) {
                    Log.d(TAG, "SUCCESS with output : " + s);
                    finishedSplits++;
                    mProgressDialog.incrementProgressBy(1);

                    if(finishedSplits == currentList.size()+1)
                        mProgressDialog.dismiss();

                    if(finishedSplits < currentList.size()) {
                        render(finishedSplits);
                    }
                    if (finishedSplits == currentList.size()) {
                        Runnable changeMessage = new Runnable() {
                            @Override
                            public void run() {
                                Log.d(TAG, "updateing massage");
                                mProgressDialog.setMessage("Rendering output video...");
                            }
                        };
                        ((MainActivity)mainActivity).runOnUiThread(changeMessage);
                        try {
                            concatenateVideosCommand();
                        } catch (IllegalArgumentException e) {
                            e.printStackTrace();
                        } catch (SecurityException e) {
                            e.printStackTrace();
                        } catch (IllegalStateException e) {
                            e.printStackTrace();
                        }
                    }


                }

                @Override
                public void onProgress(String s) {

                    //Log.d(TAG, "progress : " + s);
                }

                @Override
                public void onStart() {
                    Log.d(TAG, "Started command : ffmpeg " + command);
                }

                @Override
                public void onFinish() {

                    Log.d(TAG, "Finished command : ffmpeg " + command + " finishedSplits: " + finishedSplits + " size: " + currentList.size());

                }
            });
        } catch (FFmpegCommandAlreadyRunningException e) {
            e.printStackTrace();
        }
    }


    public static class VideoHolder{
        private Uri uri;
        private Context context;
        private long duration;
        private long start;
        private long end;
        private String rotation;
        private long id;
        VideoHolder(Context context, Uri uri, long id){
            this.uri = uri;
            this.context = context;
            this.id = id;
            MediaMetadataRetriever mdr = new MediaMetadataRetriever();
            mdr.setDataSource(context, uri);
            String res = mdr.extractMetadata(MediaMetadataRetriever.METADATA_KEY_DURATION);
            duration = Long.parseLong(res);
            res = mdr.extractMetadata(MediaMetadataRetriever.METADATA_KEY_VIDEO_ROTATION);
            rotation = res;
            start = 0;
            end  = duration;
            Log.d(TAG, "got data for uri: " + rotation);
        }

        Uri getUri(){
            return uri;
        }
        public long getId() { return id;}
        public long getDuration() { return duration; }
        public long getStart() { return start; }
        public long getEnd() { return end; }
        public void updateRange(long min, long max){
            start = min;
            end = max;
        }
        int getRealLength(){
            return Math.toIntExact(end-start);
        }
    }
}
